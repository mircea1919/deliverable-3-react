import React from 'react';

const photo = ({picName}) => {
    return (
        <div className="col-sm-6 col-md-6 col-lg-4 mb-4">
            {/*// <!--        todo: set different margins depending on resizing!-->*/}
            {/*todo: when picture is clicked, it will automatically search for this city*/}
            <img src={`cities/${picName}`} className="w-100 h-100" alt={picName}/>
        </div>
    );
}

export default photo;
