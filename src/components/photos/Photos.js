import React from 'react';
import Photo from './Photo'

const photos = () => {
    return (
        <div className="container my-5 d-flex flex-wrap">
            {/*// <!--        todo: set different margins depending on resizing!-->*/}
            <Photo picName={"rotterdam.jpg"}/>
            <Photo picName={"amsterdam.jpg"}/>
            <Photo picName={"frankfurt.jpg"}/>
            <Photo picName={"hong-kong.jpg"}/>
            <Photo picName={"hamburg.jpg"}/>
            <Photo picName={"kuala-lumpur.jpg"}/>
            <Photo picName={"london.jpg"}/>
            <Photo picName={"prague.jpg"}/>
            <Photo picName={"toronto.jpg"}/>
        </div>
    );
}

export default photos;