import React from 'react';

const distance = ({distance, city}) => {
    return (
        <div className="col-12 my-5 mx-auto text-center">
            <h1>Distance from Copenhagen to {city} : {distance} KM</h1>
        </div>
    );
}

export default distance;