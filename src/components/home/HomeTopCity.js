import React from 'react';

const homeTopCity = ({cityName, index}) => {
    return (
        <div className="col-6 bold">
            {cityName}
        </div>
    );
}

export default homeTopCity;
