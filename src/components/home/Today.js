import React from 'react';

const today = ({ city, currentWeather }) => {
    return (
        <div className="col-lg-4 d-flex flex-column justify-content-around">
            <h3 className="text-center">{city}</h3>
            <div className="d-flex justify-content-center align-items-center">
                <div className="icon-div d-inline-flex mr-3">
                    <img className="mx-auto" src={`icons/${currentWeather["WeatherIcon"]}.svg`} alt=""/>

                </div>
                <h1>{Math.round(currentWeather['Temperature']["Metric"]["Value"])}° C</h1>
            </div>
        </div>
    );
}

export default today;