import React from 'react';
import SearchForecast from './SearchForecast'
import HomeTopCity from './HomeTopCity'


const getRandomTopTenCities = () => {
    var topFiftyCities = require('./../../test/top-50-cities-temperature');

    var topTenCities = [];

    shuffle(topFiftyCities);

    topFiftyCities.slice(36).forEach(element => topTenCities.push(element["EnglishName"]));


    return topTenCities;
}

function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
}

var cities = getRandomTopTenCities();

const home = () => {
    return (

        <div>
            <div className="container my-5">
                <SearchForecast/>
            </div>


            <div className="container mb-4">
                {/*// <!--		center text on x an y axis-->*/}
                <div className="row img-weather d-flex justify-content-center align-items-center">
                    <h1>Where to tomorrow?</h1>
                </div>
            </div>

            <div className="container mb-4">
                <div className="row d-flex align-items-center">
                    <div className="col-lg-4 mb-lg-0 mb-4 mt-1">
                        <img className="w-100" src="cities/munich.jpg" alt="munich"/>

                        <h3 className="my-2">City images</h3>

                        <p>Out of ideas where to go? Click here to see a wonderful collection of city images.</p>
                        <a className="btn my-btn text-light mx-md-auto" href="photos"> Check photos </a>
                    </div>
                    <div className="col-lg-8 mb-0 mb-lg-2">
                        <h1 className="col-12 text-center">Top cities</h1>
                        {/*// <!--				display:flex // next two divs will be displayed on the same row-->*/}
                        {/*// <!--				class=row would have done the same-->*/}
                        <div className="d-flex">
                            <div className="col-lg-6">
                                <img className="w-100" src="cities/tokyo.jpg" alt="city"/>
                                <p>
                                    Check some of the biggest cities in Europe or in the world!
                                    Click here to check today's weather conditions from the most populated urban areas in the world
                                </p>
                                <a className="btn my-btn text-light" href="big-cities"> Check cities </a>
                            </div>
                            <div className="col-lg-6 d-lg-flex flex-wrap my-3">

                                {cities.map((city, i) => {
                                    return (<HomeTopCity cityName={city} index={i}/>)
                                })}
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    );
}

export default home;