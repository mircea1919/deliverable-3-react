import React from 'react';
// better than fatch because if gives us the data in the json format
import axios from 'axios';
import ForecastResults from "./ForecastResults";
import '../../test/get-city-info-munich'

class Search extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            query: '',
            results: {},
            message: null,
            value: '',
            distance: '',
            currentWeather: '',
            nextFiveDays: '',
            cityKeyId: '',
            // these should be declared somewhere else or should be dynamic
            latitudeCopenhagen: 55.658,
            longitudeCopenhagen: 11.576,
            mock: false,
            allSearches : false,
        };

        //api params should be saved somewhere else
        this.weatherapi = {
            url: 'https://dataservice.accuweather.com/',
            getCity: 'locations/v1/cities/search',
            getCurrentWeather: 'currentconditions/v1/',
            getNextFiveDays: 'forecasts/v1/daily/5day/',
            // since we have max 50 request per apikey, I divided the calls between the two apikeys available
            apiKey: 'KaXWP0BG7CSnxVc7FOBzdbUz1g68K9ki',
            // apiKey: '1GoYQGOa83u5twuZ6cM2mAenGjUnGYkc',
            secondApiKey: '1GoYQGOa83u5twuZ6cM2mAenGjUnGYkc',
            cityKey: ''

        };
        this.cancel = '';

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    // not using this method anymore
    handleOnInputChange = (event) => {
        const query = event.target.value;

        // setState is asycn; it does not guarantee us these params will be updated for the fetching
        // if we want to make sure we call the function only after all the information is set on the state, we have to put all info in the callback function!
        // that's why the getResults is called in the callback!
        this.setState({query:query, message: ''}, () => this.getCityInformation(query))
    };


    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        const query = this.state.value;
        if (query) {
            // is second param is true, we will present mock data from Munich in 15.05.2020
            this.setState({query:query, message: ''}, () => this.getCityInformation(query, false))
        }
        event.preventDefault();
    }


    getCityInformation = (cityName, mock = false) => {
        let resultMessage = null;
        const searchUrl = this.weatherapi.url + this.weatherapi.getCity + "?apikey=" +
            this.weatherapi.apiKey + "&q=" + cityName;

        if (!mock) {
            //  ################## start of request ############

            // console.log("mircea1");
            axios.get(searchUrl, {cancelToken: this.cancel.token}).then(
                res => {
                    // console.log("mircea7");
                    // console.log(res);
                    if (!res.data.length) {
                        // console.log("mircea4");
                        resultMessage = "There is no data in the search city response!";
                    }
                    // console.log("mircea5");

                    this.setState({
                        results: res.data[0],
                        message: resultMessage,
                        cityKeyId: res.data[0]["Key"],
                        distance: this.distanceBetweenCoordinates(
                            this.state.latitudeCopenhagen, this.state.longitudeCopenhagen,
                            res.data[0]["GeoPosition"]["Latitude"], res.data[0]["GeoPosition"]["Longitude"], "K"
                        )
                    }, () => this.getTodaysForecast());
                }
            ).catch(error => {
                // console.log("mircea6");
                if (axios.isCancel(error) ||  error) {
                    this.setState(
                        {
                            message: 'Failed to fetch data from the search city request',
                            // query: cityName,
                            results: {},
                        }
                    )
                }
            });

            // console.log("mircea2");
            // console.log(this.state.results);
            // console.log(this.state.message);
            //  ################## end of request ############
        } else {
            // ###########33 MOCK THE DATA ###############
            // mocking the data from json directly (I only have 50 requests per day!)
            var dataObj = require('../../test/get-city-info-munich.json');
            this.setState({
                results: dataObj[0],
                mock: true,
                message: "This is a mock result! For real api data, change the mock parameter from getCityInformation function to TRUE.",
                cityKeyId: 178086, // id key for munich!
                distance: this.distanceBetweenCoordinates(
                    this.state.latitudeCopenhagen, this.state.longitudeCopenhagen,
                    dataObj[0]["GeoPosition"]["Latitude"], dataObj[0]["GeoPosition"]["Longitude"], "K"
                )
            }, () => this.getTodaysForecast(true));


            // ########### END OF MOCK ###########
        }
    };

    // this should be in the callback of getCIty
    getTodaysForecast = (mock = false) => {

        // console.log("mircea11");

        if (!mock) {

            let resultMessage = null;
            const todaysForecastUrl = this.weatherapi.url + this.weatherapi.getCurrentWeather +
                this.state.cityKeyId + "?apikey=" + this.weatherapi.secondApiKey;

            axios.get(todaysForecastUrl, {cancelToken: this.cancel.token}).then(
                res => {
                    if (!res.data.length) {
                        resultMessage = "There is no data in the current conditions api response!";
                    }
                    this.setState({
                        currentWeather: res.data[0],
                        message: resultMessage,
                    }, () => this.getNextFiveDaysForecast());
                }
            ).catch(error => {
                if (axios.isCancel(error) ||  error) {
                    this.setState(
                        {message: 'Failed to fetch data from the current conditions api request'}
                    )
                }
            });
        } else {
            var dataObj = require('./../../test/current_weather_munich');
            this.setState({
                currentWeather: dataObj[0],
                // message: "This is a mock result!!!"
            }, () => this.getNextFiveDaysForecast(true));
        }
    };

    getNextFiveDaysForecast = (mock=false) => {
        if (!mock) {
            let resultMessage = null;
            const nextFiveDaysForecastUrl = this.weatherapi.url + this.weatherapi.getNextFiveDays +
                this.state.cityKeyId + "?apikey=" + this.weatherapi.secondApiKey;

            axios.get(nextFiveDaysForecastUrl, {cancelToken: this.cancel.token}).then(
                res => {
                    if (!res.data.length) {
                        resultMessage = "There is no data in the next five days api response!";
                    }
                    this.setState({
                        nextFiveDays: res.data,
                        message: resultMessage,
                        allSearches: true
                    });
                }
                // we check the error here!!!!
            ).catch(error => {
                if (axios.isCancel(error) ||  error) {
                    this.setState(
                        {
                            message: 'Failed to fetch data from the current conditions api request',
                            allSearches: true
                        }
                    )
                }
            });
        } else {
            // ###########33 MOCK THE DATA ###############
            var dataObj = require('./../../test/next_five_days_munich');
            this.setState({
                nextFiveDays: dataObj
            });
            // ########### END OF MOCK ###########
        }
    };


    distanceBetweenCoordinates(lat1, lon1, lat2, lon2, unit) {
        var radlat1 = Math.PI * lat1/180
        var radlat2 = Math.PI * lat2/180
        var theta = lon1-lon2
        var radtheta = Math.PI * theta/180
        var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
        dist = Math.acos(dist)
        dist = dist * 180/Math.PI
        dist = dist * 60 * 1.1515
        if (unit=="K") { dist = dist * 1.609344 }
        if (unit=="N") { dist = dist * 0.8684 }
        return Math.round(dist)
    }

    // fToCelsius(f) {
    //     return (5/9) * (f-32);
    // }






    render() {

        const {results} = this.state;
        const {distance} = this.state;

        return (
            <div>
                <div className="row forecast">
                    <div className="col-md-12">
                        <form className="forecast-form" onSubmit={this.handleSubmit}>
                            <div className="input-group">
                                {/*it will call the handleOnInputCHange whenever we type something*/}
                                <input
                                    type="text"
                                    className="form-control"
                                    id="search-input"
                                    // value={query}
                                    // onChange={this.handleOnInputChange}
                                    name = "query"
                                    placeholder="Search for a city..."
                                    value={this.state.value}
                                    onChange={this.handleChange}

                                />
                                <div className="input-group-append">
                                    <input
                                        type="submit"
                                        className="btn my-btn text-light"
                                        value="Find"
                                    />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                {(results['EnglishName'] != undefined && this.state.currentWeather != '' && this.state.nextFiveDays != '') ? (
                    <ForecastResults city={results["EnglishName"]} distance={distance} currentWeather = {this.state.currentWeather}
                                     nextFiveDays = {this.state.nextFiveDays} />
                ) : (
                    <div>
                        {(this.state.message != null && this.state.mock !== true && this.state.query !== '') ? (
                            <div className="text-center my-5">
                                <h1>There are no results for "{this.state.query}"</h1>
                            </div>
                        ) : (
                            // nothiing
                            <div></div>
                        )}
                    </div>
                )}

                {(this.state.mock === true) ? (
                    <div className="text-center">
                        <h1 className="text-danger">{this.state.message}.</h1>
                        {/*<h2></h2>*/}
                    </div>
                ) : (
                    <div></div>
                )}

            </div>
        );
    }

}

export default Search;