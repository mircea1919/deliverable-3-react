import React from 'react';
import Today from './Today'
import NextFiveDays from './NextFiveDays'
import Distance from './Distance'

const forecastResults = ({ city, distance, currentWeather, nextFiveDays }) => {
    return (
            <div className="row my-4">
                <Distance distance={distance} city={city}/>
                <Today city={city} currentWeather={currentWeather}/>
                <NextFiveDays nextFiveDays = {nextFiveDays}/>
            </div>
    );
}

export default forecastResults;