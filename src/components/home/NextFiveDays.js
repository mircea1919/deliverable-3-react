import React from 'react';
import Day from './Day'

const nextFiveDays = ({nextFiveDays}) => {

    const nextDaysArray = nextFiveDays["DailyForecasts"]
    return (
        <div className="col-lg-8">
            <h2> Next 5 days: </h2>
            <div className="d-flex justify-content-between next-days">
                {nextDaysArray.map((day, i) => {
                    return (<Day dailyForecast={day} index={i}/>)
                })}
            </div>
        </div>
    );
}

export default nextFiveDays;