import React from 'react';


const day = ({dailyForecast, index}) => {
    return (
        <div className="d-flex flex-column align-items-center">

            <p>Day {index + 1}</p>
            <div className="icon-div d-flex mb-2">
                <img className="mx-auto" src={`icons/${dailyForecast["Day"]["Icon"]}.svg`} alt=""/>
            </div>

            {/*formula for calculating celsius degrees from farhenheit*/}
            {/*todo: how to create/use a function inside a functional component?*/}
            <div>min: {Math.round((5/9) * (dailyForecast["Temperature"]["Minimum"]["Value"] - 32))} ° C </div>
            <div>max: {Math.round((5/9) * (dailyForecast["Temperature"]["Maximum"]["Value"]- 32))} ° C </div>
        </div>
    );
}

export default day;