import React from 'react';

const contact = () => {
    return (
        <div className="container">
            <div className="row my-5">
                <div className="col-md-6">
                    <iframe className="w-100"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2247.803464887802!2d12.570138315794116!3d55.70978790277628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x465252f67ea29223%3A0xe3476771516b59f4!2sT%C3%A5singegade%20Kollegiet!5e0!3m2!1sen!2sde!4v1590181839306!5m2!1sen!2sde"
                        width="600" height="450" frameBorder="0" allowFullScreen=""
                        aria-hidden="false" tabIndex="0">
                    </iframe>
                </div>
                <div className="col-md-6 row">
                    <div className="col-6 my-auto">
                        <h2>Name:</h2>
                        <h3>Email:</h3>
                        <h4>Phone:</h4>
                    </div>
                    <div className="col-6 my-auto">
                        <h2>Mircea Murasan</h2>
                        <h3>mirm@itu.dk</h3>
                        <h3>+4560245551</h3>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default contact;