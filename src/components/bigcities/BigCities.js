import React from 'react';
import axios from "axios";
import City from './City'

class BigCities extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            bigCities: {},
            message: ''
        };

        //api params should be saved somewhere else
        this.weatherapi = {
            url: 'https://dataservice.accuweather.com/',
            getCity: 'locations/v1/cities/search',
            getCurrentWeather: 'currentconditions/v1/',
            getNextFiveDays: 'forecasts/v1/daily/5day/',
            getTopFiftyCities: 'currentconditions/v1/topcities/50',
            apiKey: 'KaXWP0BG7CSnxVc7FOBzdbUz1g68K9ki',
            // apiKey: '1GoYQGOa83u5twuZ6cM2mAenGjUnGYkc',
            secondApiKey: '1GoYQGOa83u5twuZ6cM2mAenGjUnGYkc',
            cityKey: ''

        };
        this.cancel = '';

        // this.getBigCitiesData();

        //mock data
        // this.getBigCitiesData(true);

        // console.log("mircea2");
        // console.log(this.state.bigCities);

    }

    // use componentWIllMount to fetch the data before the render!
    componentWillMount() {
        // if param is true, then we are mocking the data! param should be false!
        this.getBigCitiesData(false);
    }


    getBigCitiesData = (mock = false) => {

        if (!mock) {
            let resultMessage = null;
            const topCitiesUrl = this.weatherapi.url + this.weatherapi.getTopFiftyCities + "?apikey=" + this.weatherapi.apiKey;

            axios.get(topCitiesUrl, {cancelToken: this.cancel.token}).then(
                res => {
                    if (!res.data.length) {
                        resultMessage = "There is no data in the top cities api response!";
                    }

                    // console.log("mircea10");
                    // console.log(res);
                    // console.log(res.data);

                    this.setState({
                        bigCities : res.data,
                        message : resultMessage
                        }

                    );

                    // this.state.bigCities = res.data;
                    // this.state.message = resultMessage;


                    // console.log("mircea11");
                    // console.log(this.state.bigCities);

                    // this.setState({
                    //     currentWeather: res.data,
                    //     message: resultMessage,
                    // });
                }
            ).catch(error => {
                if (axios.isCancel(error) ||  error) {
                    // console.log("mircea13");
                    this.setState(
                        {message: 'Failed to fetch data from the top cities api request'}
                    )
                }
            });

            // console.log("mircea12");
            // console.log(this.state.bigCities);

        } else {
            // console.log("mircea3");
            var dataObj = require('./../../test/top-50-cities-temperature');
            // console.log("mircea4");
            // console.log(dataObj);
            this.state.bigCities = dataObj;

        }
    };
    render() {
        const {bigCities} = this.state;

        // console.log("mircea5");
        // console.log(bigCities);
        return (
            <div className="container my-5 d-flex flex-wrap">
                {/*there should be a better option to do the if!*/}
                {(bigCities.length > 0 ) ? (
                    bigCities.map((city, i) => {
                            return (<City city={city} index={i}/>)
                        })
                ) : (
                    <div></div>
                )}
            </div>
        );
    }
}
// const BigCities = () => {
// return (
//     <div className="container">
//     </div>
// );
// }

export default BigCities;
