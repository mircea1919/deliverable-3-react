import React from 'react';

const city = ({city, index}) => {
    return (
        <div className="col-sm-6 col-md-4 col-lg-3 mb-4 my-5 my-md-3 d-flex flex-column align-items-center">
            <h2>{city["LocalizedName"]}</h2>
            <div>{city["Country"]["LocalizedName"]}</div>
            <div className="icon-div d-flex my-md-3">
                <img className="mx-auto" src={`icons/${city["WeatherIcon"]}.svg`} alt=""/>
            </div>
            <h3>{city["Temperature"]["Metric"]["Value"]}° C</h3>

        </div>
    );
}

export default city;