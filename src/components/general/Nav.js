import React from 'react';

const nav = () => {
    return (
        <nav className="navbar navbar-dark navbar-expand-lg">

            <div className="container">
                <a className="" href="#">
                    <img src="logo.svg" alt=""/>
                </a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
                        aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarText">
                    {/*// <!--					todo: add justify content!-->*/}
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="/">Home <span
                                className="sr-only">(current)</span></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="photos">Photos</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="big-cities">Big Cities</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="contact">Contact</a>
                        </li>
                        {/*<li className="nav-item">*/}
                        {/*    <a className="nav-link" href="terms">Terms and Conditions</a>*/}
                        {/*</li>*/}
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default nav;