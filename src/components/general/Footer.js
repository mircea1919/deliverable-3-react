import React from 'react';

const footer = () => {
    return (
        <footer className="page-footer font-small bg-dark pt-4 text-light">
            <div className="container">
                <div
                    className="row d-flex justify-content-xl-around justify-content-md-between justify-content-md-center">
                    {/*// <!--                order will change on screen smaller than md!-->*/}
                    <div className="col-sm-3 order-md-1 order-3">
                        <ul>
                            <li className="mb-3 font-weight-bold">About React:</li>
                            <li>
                                <a href="https://reactjs.org/" target="_blank">Oficial website</a>
                            </li>
                            <li>
                                <a href="https://github.com/facebook/react" target="_blank">Github</a>
                            </li>
                            <li>
                                <a href="https://www.w3schools.com/REACT/default.asp" target="_blank">W3schools</a>
                            </li>
                            <li>
                                <a href="https://en.wikipedia.org/wiki/React_(web_framework)" target="_blank">Wikipedia</a>
                            </li>
                        </ul>
                    </div>

                    <div className="col-sm-3 order-md-2 order-1">
                        <ul>
                            <li className="mb-3 font-weight-bold">API Documentation</li>
                            <li>
                                <a href="https://developer.accuweather.com/accuweather-locations-api/apis/" target="_blank">Location API</a>
                            </li>
                            <li>
                                <a href="https://developer.accuweather.com/accuweather-current-conditions-api/apis" target="_blank">Current conditions</a>
                            </li>
                            <li>
                                <a href="https://developer.accuweather.com/accuweather-forecast-api/apis" target="_blank">Forecast API</a>
                            </li>
                            <li>
                                <a href="https://developer.accuweather.com/accuweather-current-conditions-api/apis/get/currentconditions/v1/topcities/%7Bgroup%7D" target="_blank">Top cities API</a>
                            </li>
                        </ul>
                    </div>

                    <div className="col-sm-3 order-md-3 order-2">
                        <ul>
                            <li className="mb-3 font-weight-bold">Photos source</li>
                            <li>
                                <a href="https://pixabay.com/" target="_blank">Pixbay</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <div className="footer-copyright text-center py-3">
                © 2020 Copyright: Mircea Murasan
            </div>

        </footer>
    );
};

export default footer;