import React from 'react';
import logo from './logo.svg';
import './App.css';


// npm install --save bootstrap
// npm install jquery
// npm install popper.js --save
import "jquery/dist/jquery"
// import 'bootstrap/dist/js/popper.min.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap';
import './assets/css/styles.css'
import './assets/css/variables.css'
// import './assets/js/bootstrap.min'
// import './assets/js/jquery.min'


import { BrowserRouter, Route, Switch } from 'react-router-dom';

import Home from './components/home/Home';
import Contact from './components/contact/Contact';
import Photos from './components/photos/Photos'
import Terms from './components/terms/Terms'
import Nav from './components/general/Nav'
import Footer from './components/general/Footer'
import BigCities from  './components/bigcities/BigCities'

function App() {
  return (
      <BrowserRouter>
        <div>
          <Nav />
          <Switch>
            <Route path="/" component={Home} exact/>
            <Route path="/photos" component={Photos}/>
            <Route path="/big-cities" component={BigCities}/>
            <Route path="/contact" component={Contact}/>
            {/*<Route path="/terms" component={Terms}/>*/}
            <Route component={Error}/>
          </Switch>
        <Footer />
        </div>
      </BrowserRouter>
    // <div className="App">
    //   <header className="App-header">
    //     <img src={logo} className="App-logo" alt="logo" />
    //     <p>
    //       Edit <code>src/App.js</code> and save to reload.
    //     </p>
    //     <a
    //       className="App-link"
    //       href="https://reactjs.org"
    //       target="_blank"
    //       rel="noopener noreferrer"
    //     >
    //       Learn React
    //     </a>
    //   </header>
    // </div>
  );
}

export default App;
